# iPyParallel - Intro to Parallel Programming
from ipyparallel import Client
import numpy as np
import time
from matplotlib import pyplot as plt

# Problem 1
def initialize():
    """
    Write a function that initializes a Client object, creates a Direct
    View with all available engines, and imports scipy.sparse as spar on
    all engines. Return the DirectView.
    """
    client = Client() #Start a client
    dview = client[:]
    dview.execute("import scipy.sparse as sparse") #import scipy

    return dview
    
# Problem 2
def variables(dx):
    """
    Write a function variables(dx) that accepts a dictionary of variables. Create
    a Client object and a DirectView and distribute the variables. Pull the variables back and
    make sure they haven't changed. Remember to include blocking.
    """
    client = Client() #Start a client
    dview = client[:]
    dview.block = True #Don't know what blocking is but will learn late rin lab
    
    dview.push(dx)
    
    for key in dx.keys():
        for val in dview.pull(key): #Check all the keys
            if val != dx[key]:
                print("Not same")
        

#test_dictionary = {'a': 14, 'b': 16, 'c' : 18, 'd': 78}
#variables(test_dictionary)



# Problem 3
def prob3(n=1000000):
    """
    Write a function that accepts an integer n.
    Instruct each engine to make n draws from the standard normal
    distribution, then hand back the mean, minimum, and maximum draws
    to the client. Return the results in three lists.
    
    Parameters:
        n (int): number of draws to make
        
    Returns:
        means (list of float): the mean draws of each engine
        mins (list of float): the minimum draws of each engine
        maxs (list of float): the maximum draws of each engine.
    """
    client = Client()
    dview = client[:]

    dview.execute("import numpy as np")
    dview.execute("normal = np.random.normal(0,1," + str(n) + ")") #Draw n times from standard normal
    dview.execute("mean =  normal.mean()")
    dview.execute("mini =  np.min(normal)")
    dview.execute("maxi =  np.max(normal)")
    
    
    return dview['mean'], dview['mini'], dview['maxi'] #return 3 lists


# Problem 4
def prob4():
    """
    Time the process from the previous problem in parallel and serially for
    n = 1000000, 5000000, 10000000, and 15000000. To time in parallel, use
    your function from problem 3 . To time the process serially, run the drawing
    function in a for loop N times, where N is the number of engines on your machine.
    Plot the execution times against n.
    """
    client = Client()
    Ns = [1000000, 5000000, 10000000, 15000000] #Groups of N
    parallel_times = []
    serial_times = []
    for N in Ns:
        #Test in Parallel
        start = time.time()
        prob3(N)
        end = time.time()
        parallel_times.append(end - start)
        
        start = time.time() #Time it for each group of 4
        for j in range(len(client.ids)):
            normal = np.random.normal(0,1,N) #draw N times
            mean = normal.mean()
            mini = np.min(normal)
            maxi = np.max(normal)
        end = time.time()
        
        serial_times.append(end-start)
            
    plt.plot(Ns, parallel_times, label = "Parallel")
    plt.plot(Ns, serial_times, label = "Serial")
    plt.legend()
    plt.xlabel("n")
    plt.ylabel("time") #don't forget labels
    plt.show()
            
    return
#prob4()

# Problem 5
def parallel_trapezoidal_rule(f, a, b, n=200):
    """
    Write a function that accepts a function handle, f, bounds of integration,
    a and b, and a number of points to use, n. Split the interval of
    integration among all available processors and use the trapezoidal
    rule to numerically evaluate the integral over the interval [a,b].

    Parameters:
        f (function handle): the function to evaluate
        a (float): the lower bound of integration
        b (float): the upper bound of integration
        n (int): the number of points to use; defaults to 200
    Returns:
        value (float): the approximate integral calculated by the
            trapezoidal rule
    """
    client = Client()
    dview =  client[:]
    num_engines = len(client.ids)
    points_per_engine = n / num_engines
    
    h = (b-a) / n

    x = [i*h for i in range(n +1)]
 
    dview.scatter("nums", x) #Give an equal number of nums to each one
    dview.push({'f': f, 'h': h})
    dview.execute("sum1 = (h/2) * np.sum([f(nums[i]) + f(nums[i+1]) for i in range(len(nums)-1)])")
    #Set up the intervals/linspace
    sums = dview['sum1']

    result = np.sum(sums)
    return result #Result will still be slightly different because some intervals should have been counted twice. 

    















"""
str1 = str(delta * points_per_engine * (i+1))
        str2 = str(points_per_engine)
        str3 = "interval1 = np.linspace(begin," + str1 + ", " + str2 + ")"
        client[i].execute(str3)
        print(client[i]['interval1'])

    dview.execute("import numpy as np")
    dview.execute("intervals = np.linspace(a," + str1 + ", " + str2 + ")" )
    print(dview['intervals'])
    """